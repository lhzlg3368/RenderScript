package renderscript;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.RenderScript;
import android.util.Log;
import android.widget.ImageView;


public class MainActivity extends AppCompatActivity {
    private Allocation aIn;
    private Allocation aOut;
    private Bitmap mInBitmap;
    private ScriptC_hello mScript;

    private ImageView mSrcImageView;
    private ImageView mDstImageView;
    private Bitmap mOutBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSrcImageView = (ImageView) findViewById(R.id.src);
        mDstImageView = (ImageView) findViewById(R.id.dst);

        mInBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ttt);
        mOutBitmap = Bitmap.createBitmap(mInBitmap.getWidth(), mInBitmap.getHeight(), mInBitmap.getConfig());
        mSrcImageView.setImageBitmap(mInBitmap);

        RenderScript rs = RenderScript.create(this);
        mScript = new ScriptC_hello(rs);

        aIn = Allocation.createFromBitmap(rs, mInBitmap);
        aOut = Allocation.createFromBitmap(rs, mInBitmap);

        Log.w("Log", "" + aIn.getBytesSize());

        mScript.forEach_invert(aIn, aOut);
        aOut.copyTo(mOutBitmap);
        mDstImageView.setImageBitmap(mOutBitmap);
        rs.destroy();
    }
}
